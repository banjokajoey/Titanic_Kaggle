# Feature metadata container for classifier initialization

class FeatureMetadata(object):

    class Type(object): # TODO: enum module not found?
        DISCRETE = 0
        CONTINUOUS = 1


    def __init__(self, field_name, data_type):
        self.field_name = field_name
        self.data_type = data_type


# Abstract classifier class

class Classifier(object):

    # Constructor

    def __init__(self, num_classes, feature_metadata):
        self.num_classes = num_classes
        self.feature_metadata = feature_metadata


    # Public interface

    def train(self, training_vecs, labels):
        raise NotImplementedError()


    def classify(self, feature_vec):
        raise NotImplementedError()


    # Helper functions

    def index_of_max(self, array):
        if len(array) == 0:
            return -1
        best_index = 0
        max_value = array[0]
        for i, value in enumerate(array):
            if value > max_value:
                best_index = i
                max_value = value
        return best_index