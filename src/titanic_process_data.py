import csv_lib
import re
import titanic_helpers


# Titanic data processing functions
   
def titanic_cabin_num_to_floor_num(room_num):
    floor_num = ""
    if len(room_num) > 0:
        floor_char = ord(room_num[0])
        if floor_char >= ord('A') and floor_char <= ord('G'):
            floor_num = floor_char - ord('A') + 1
    return floor_num


def titanic_add_col_is_miss(col_name, row):
    regex_str = '(^|\ )(miss|mlle|ms)(\ |-|\.)'
    passenger_name = row['Name']
    row[col_name] = 1 if re.search(regex_str, passenger_name, re.IGNORECASE) else 0


def titanic_add_col_has_title(col_name, row):
    regex_str = '(^|\ )(dr|master|major|count|countess|rev)(\ |-|\.)'
    passenger_name = row['Name']
    row[col_name] = 1 if re.search(regex_str, passenger_name, re.IGNORECASE) else 0


# Process CSV file

def process_titanic_csv(in_filepath, out_filepath):
    csv = csv_lib.CSVManager()
    csv.read(in_filepath, titanic_helpers.titanic_row_is_valid)
    csv.col_map('Sex', ['male', 'female'], [0, 1])
    csv.col_map('Embarked', ['Q', 'S', 'C'], [1, 2, 3])
    csv.col_normalize('Age')
    csv.col_normalize('Fare')
    csv.col_transform('Cabin', titanic_cabin_num_to_floor_num, 'Floor')
    csv.new_col('Miss', titanic_add_col_is_miss)
    csv.new_col('Title', titanic_add_col_has_title)
    csv.remove_col('Name')
    csv.remove_col('Ticket')
    csv.save(out_filepath)


# Main function

def main():
    process_titanic_csv('../data/pristine/train.csv', '../data/processed/train_processed.csv')
    process_titanic_csv('../data/pristine/test.csv', '../data/processed/test_processed.csv')


# Script handle

if __name__ == '__main__':
    main()