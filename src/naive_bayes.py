from classifier import Classifier, FeatureMetadata
import math


# Naive Bayes classifier class

class NaiveBayesClassifier(Classifier):

    # Constructor

    def __init__(self, num_classes, feature_metadata):
        # TODO: Having trouble super-initing...
        Classifier.__init__(self, num_classes, feature_metadata)
        self.pCalculators = {}
        for metadata in feature_metadata:
            pCalc = None
            if metadata.data_type == FeatureMetadata.Type.DISCRETE:
                pCalc = DiscreteProbCalculator(num_classes)
            elif metadata.data_type == FeatureMetadata.Type.CONTINUOUS:
                pCalc = ContinuousProbCalculator(num_classes)
            self.pCalculators[metadata.field_name] = pCalc


    # Overridden abstract methods

    def train(self, feature_vecs, labels):
        # Tally feature frequency within feature prob calculators
        class_freqs = [0] * self.num_classes
        for vec_idx, vec in enumerate(feature_vecs):
            label = labels[vec_idx]
            class_freqs[label] += 1
            for feature_name, value in vec.items():
                if feature_name in self.pCalculators:
                    self.pCalculators[feature_name].register_value_mapping(label, value)

        # Convert class frequency counts to their log probabilities
        self.class_log_probs = []
        total_num_labels = len(labels)
        for i in range(self.num_classes):
            probability = class_freqs[i] / float(total_num_labels)
            self.class_log_probs.append(math.log(probability))
        

    def classify(self, feature_vec):
        class_predictions = []
        for class_idx in range(self.num_classes):
            predicted_log_prob = 0.0
            for feature_name, feature_val in feature_vec.items():
                pCalc = self.pCalculators[feature_name]
                predicted_log_prob += pCalc.get_class_log_p(class_idx, feature_val)
            predicted_log_prob += self.class_log_probs[class_idx]
            class_predictions.append(predicted_log_prob)
        return self.index_of_max(class_predictions)


# Feature probability manager ABC

class FeatureProbCalculator(object):

    # Public interface

    def register_value_mapping(self, class_idx, value):
        raise NotImplementedError()


    def get_class_log_p(self, class_idx, value):
        raise NotImplementedError()


# Discrete feature probability manager

class DiscreteProbCalculator(FeatureProbCalculator):

    # Constructor

    def __init__(self, num_classes):
        self.value_counts = [{ '__total' : 0 } for i in range(num_classes)]


    # Overridden abstract methods

    def register_value_mapping(self, class_idx, value):
        value_counter = self.value_counts[class_idx]
        value_counter[value] = value_counter[value] + 1 if value in value_counter else 1
        value_counter['__total'] += 1


    def get_class_log_p(self, class_idx, value):
        value_counter = self.value_counts[class_idx]
        count = value_counter[value] if value in value_counter else 0.01
        total = max(value_counter['__total'], 1)
        return math.log(count / float(total))


# Continuous feature probability manager

class ContinuousProbCalculator(FeatureProbCalculator):

    # Constructor

    def __init__(self, num_classes):
        self.class_values = [[] for i in range(num_classes)]
        self.gaussians = [GaussianCalculator(0.5, 0.25) for i in range(num_classes)]
        self.gaussians_are_stale = [False for i in range(num_classes)]


    # Overridden abstract methods

    def register_value_mapping(self, class_idx, value):
        if isinstance(value, float):
            self.class_values[class_idx].append(value)
            self.gaussians_are_stale[class_idx] = True


    def get_class_log_p(self, class_idx, value):
        gaussian = self.gaussians[class_idx]
        if self.gaussians_are_stale[class_idx]:
            gaussian.calculate_distribution(self.class_values[class_idx])
            self.gaussians_are_stale[class_idx] = False
        return math.log(max(gaussian.get_value_prob(value), 0.00001))


# Gaussian calculator class

class GaussianCalculator(object):

    def __init__(self, mean = 0, std_dev = 1):
        self.mean = mean
        self.std_dev = std_dev


    def calculate_distribution(self, values):
        self.mean = sum(values) / len(values)
        self.std_dev = 0
        for val in values:
            self.std_dev += math.pow(val - self.mean, 2)
        self.std_dev = math.sqrt(self.std_dev / len(values)) 


    def get_value_prob(self, value):
        if not isinstance(value, float):
            value = self.mean
        min_val = value - 0.2 * self.std_dev
        max_val = value + 0.2 * self.std_dev
        return self.gaussian_cdf(max_val) - self.gaussian_cdf(min_val)


    def gaussian_cdf(self, value):
        if self.std_dev == 0:
            return 1 if value == self.mean else 0
        z = (value - self.mean) / self.std_dev
        return self.__gaussian_std_cdf(z)


    def __gaussian_std_cdf(self, value):
        return (1.0 + math.erf(value / math.sqrt(2.0))) / 2.0


    def __crunch_values(self):
        pass
        self.metadata_is_stale = False