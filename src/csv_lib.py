import csv
import random


# CSV Manipulation class

class CSVManager(object):

    # Constructor

    def __init__(self):
        pass


    # File I/O

    def read(self, in_filepath, row_validator=None):
        with open(in_filepath, 'r') as in_file:
            self.data = []
            self.col_names = [h.strip() for h in in_file.next().split(',')]
            csv_reader = csv.DictReader(in_file, fieldnames=self.col_names)
            for row in csv_reader:
                if row_validator:
                    if not row_validator(row):
                        continue
                for key, value in row.items():
                    row[key] = value.strip()
                self.data.append(row)


    def save(self, out_filepath):
        with open(out_filepath, 'w') as out_file:
            csv_writer = csv.DictWriter(out_file, fieldnames=self.col_names)
            csv_writer.writeheader()
            for row in self.data:
                csv_writer.writerow(row)


    # Single column value manipulation

    def col_normalize(self, col_name):
        min_val = 0
        max_val = 0
        for row in self.data:
            curr_val = row[col_name]
            if curr_val:
                curr_val = float(curr_val)
                if curr_val < min_val:
                    min_val = curr_val
                if curr_val > max_val:
                    max_val = curr_val

        value_range = max_val - min_val
        if value_range == 0:
            return

        for row in self.data:
            curr_val = row[col_name]
            if curr_val:
                curr_val = float(curr_val)
                row[col_name] = round((curr_val - min_val) / value_range, 5)


    def col_map(self, col_name, keys, values):
        for row in self.data:
            curr_val = row[col_name]
            for i in range(len(keys)):
                if curr_val == keys[i]:
                    row[col_name] = values[i]
                    break


    def col_transform(self, col_name, transform_fn, new_name=None):
        for row in self.data:
            row[col_name] = transform_fn(row[col_name])
        if new_name:
            self.rename_col(col_name, new_name)


    # Column manipulation

    def new_col(self, col_name, init_fn):
        self.col_names.append(col_name)
        for row in self.data:
            init_fn(col_name, row)


    def remove_col(self, col_name):
        self.col_names.remove(col_name)
        for row in self.data:
            del row[col_name]


    def rename_col(self, old_name, new_name):
        for row in self.data:
            row[new_name] = row[old_name]
            del row[old_name]
        for i, col_name in enumerate(self.col_names):
            if col_name == old_name:
                self.col_names[i] = new_name
                break


    def swap_cols(self, col_name_1, col_name_2):
        index_1 = self.col_names.index(col_name_1)
        index_2 = self.col_names.index(col_name_2)
        self.col_names[index_1] = col_name_2
        self.col_names[index_2] = col_name_1


    # Row manipulation

    def shuffle_rows(self):
        random.shuffle(self.data)


    # Data manipulation

    def make_numeric(self):
        try:
            for row in self.data:
                for key, value in row.items():
                    if value != '':
                        row[key] = float(value) if '.' in value else int(value)
        except ValueError:
            print("CSVManager attempted to make a nonnumeric value numeric")


    # Data access

    def num_rows(self):
        return len(self.data)


    def get_col(self, col_name):
        values = []
        if col_name in self.col_names:
            for row in self.data:
                values.append(row[col_name])
        return values


    def get_data(self):
        return self.data