from classifier import FeatureMetadata as FM
from naive_bayes import NaiveBayesClassifier
from knn import KNearestNeighborClassifier
import csv_lib
import math
import random
import titanic_helpers


# Titanic Machine Learning class

class TitanicML(object):

    # Constructor

    def __init__(self, classifier_type):
        self.classifier_type = classifier_type
        self.num_classes = 2
        self.feature_metadata = []
        self.feature_metadata.append(FM('Pclass',   FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Sex',      FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Age',      FM.Type.CONTINUOUS))
        self.feature_metadata.append(FM('SibSp',    FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Parch',    FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Fare',     FM.Type.CONTINUOUS))
        self.feature_metadata.append(FM('Floor',    FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Embarked', FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Miss',     FM.Type.DISCRETE))
        self.feature_metadata.append(FM('Title',    FM.Type.DISCRETE))


    def __new_classifier(self):
        if self.classifier_type == 'naive_bayes':
            return NaiveBayesClassifier(self.num_classes, self.feature_metadata)
        if self.classifier_type == 'knn':
            return KNearestNeighborClassifier(self.num_classes, self.feature_metadata, 5)
        raise NotImplementedError()


    # Train

    def train(self, training_csv_filepath, num_cross_validations):
        csv = csv_lib.CSVManager()
        csv.read(training_csv_filepath, titanic_helpers.titanic_row_is_valid)
        csv.make_numeric()
        csv.shuffle_rows()

        all_labels = csv.get_col('Survived')
        csv.remove_col('Survived')
        csv.remove_col('PassengerId')
        all_training_vecs = csv.get_data()

        num_samples = csv.num_rows()
        partition_size = int(math.ceil(num_samples / float(num_cross_validations)))
        accuracies = []
        for i in range(num_cross_validations):
            start_i = i * partition_size
            end_i = min(start_i + partition_size, num_samples)

            train_vecs, test_vecs = self.__slice_training_data(all_training_vecs, start_i, end_i)
            train_labels, test_labels = self.__slice_training_data(all_labels, start_i, end_i)

            classifier = self.__new_classifier()
            classifier.train(train_vecs, train_labels)
            accuracy = self.test(classifier, test_vecs, test_labels)
            accuracies.append(accuracy)
        self.report_training_accuracy(accuracies)


    def __slice_training_data(self, data, slice_start, slice_end):
        test = data[slice_start : slice_end]
        train = data[ : slice_start] + data[slice_end : ]
        return (train, test)


    # Test

    def test(self, classifier, test_vecs, test_labels):
        num_correct = 0
        for i, test_vec in enumerate(test_vecs):
            prediction = classifier.classify(test_vec)
            label = test_labels[i]
            if prediction == label:
                num_correct += 1
        return num_correct / float(len(test_vecs))


    # Analyze

    def analyze(self, testing_csv_filepath):
        csv = csv_lib.CSVManager()
        csv.read(testing_csv_filepath, titanic_helpers.titanic_row_is_valid)
        csv.make_numeric()


    # Report

    def report_training_accuracy(self, accuracies):
        average = round(sum(accuracies) / len(accuracies), 2)
        rounded_accuracies = [ round(val, 2) for val in accuracies ]
        rounded_accuracies.sort()

        print ("----------------------------------------")
        print ("TRAINING ACCURACY ({0})".format(self.classifier_type))
        print ("----------------------------------------")
        print ("avg:\t{0}".format(average))
        print ("min:\t{0}".format(min(rounded_accuracies)))
        print ("max:\t{0}".format(max(rounded_accuracies)))
        accuracies_str = ''
        for val in rounded_accuracies:
            accuracies_str += str(val)
            accuracies_str += ' '
        print ("values: {0}".format(accuracies_str))
        print ("cross_validations: {0}".format(len(accuracies)))


# Main function

def main():
    classifier_type = 'knn'
    analyzer = TitanicML(classifier_type)
    analyzer.train('../data/processed/train_processed.csv', 10)


# Script handle

if __name__ == '__main__':
    random.seed(0)
    main()