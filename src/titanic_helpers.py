def titanic_row_is_valid(row):
    if not row:
        return False
    passenger_id = row['PassengerId']
    return passenger_id != None and passenger_id.strip() != ''