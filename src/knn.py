from classifier import Classifier
import math


# Nearest Neighbor classifier class

class KNearestNeighborClassifier(Classifier):

    # Constructor

    def __init__(self, num_classes, feature_metadata, k=1):
        # TODO: Having trouble super-initing...
        Classifier.__init__(self, num_classes, feature_metadata)
        self.neighborhoods = [[] for i in range(num_classes)]
        self.k = k


    # Overriden abstract methods

    def train(self, feature_vecs, labels):
        for i, vec in enumerate(feature_vecs):
            self.neighborhoods[labels[i]].append(vec)


    def classify(self, feature_vec):
        # Get k nearest neighbors
        nearest_neighbors = []
        for class_idx, neighborhood in enumerate(self.neighborhoods):
            for citizen in neighborhood:
                distance = euclidean_distance(feature_vec, citizen)
                if len(nearest_neighbors) < self.k:
                    nearest_neighbors.append((class_idx, distance))
                elif distance < nearest_neighbors[-1][1]:
                    nearest_neighbors[-1] = (class_idx, distance)
                    nearest_neighbors.sort(key=lambda neighbor: neighbor[1])

        # Classify by nearest neighbors
        class_scores = [0] * self.num_classes
        for class_idx, distance in nearest_neighbors:
            class_scores[class_idx] += 1
        return self.index_of_max(class_scores)


# Distance function

def euclidean_distance(vec_1, vec_2):
    sq_sum = 0.0
    for key, value in vec_1.items():
        if value != '' and key in vec_2 and vec_2[key] != '':
            sq_sum += (vec_2[key] - vec_1[key]) ** 2
    return math.sqrt(sq_sum)