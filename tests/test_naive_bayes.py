import unittest
from src import naive_bayes as nb
from src import classifier as c


# Naive Bayes Unit Tests

class TestNaiveBayes(unittest.TestCase):

    # Classifier test function

    def run_naive_bayes_classifier(self, num_classes, feature_metadata, train_data, train_labels, test_data, test_labels):
        classifier = nb.NaiveBayesClassifier(num_classes, feature_metadata)
        classifier.train(train_data, train_labels)

        for i, test_vec in enumerate(test_data):
            prediction = classifier.classify(test_vec)
            self.assertEqual(prediction, test_labels[i])


    # Test discrete inputs

    def test_bayes_one_discrete_feature(self):
        train_data   = [{ 'a' : 0 }, { 'a' : 1 }]
        train_labels = [0, 1]
        test_data    = [{ 'a' : 0 }, { 'a' : 1 }]
        test_labels  = [0, 1]

        feature_metadata = []
        feature_metadata.append(c.FeatureMetadata('a', c.FeatureMetadata.Type.DISCRETE))
        self.run_naive_bayes_classifier(2, feature_metadata, train_data, train_labels, test_data, test_labels)


    def test_bayes_many_discrete_features(self):
        train_data   = [
            { 'a' : 0, 'b' : 0, 'c' : 0 }, 
            { 'a' : 1, 'b' : 1, 'c' : 1 }
        ]
        train_labels = [0, 1]
        test_data    = [
            { 'a' : 1, 'b' : 0, 'c' : 1 },
            { 'a' : 0, 'b' : 1, 'c' : 0 } 
        ]
        test_labels  = [1, 0]

        feature_metadata = []
        feature_metadata.append(c.FeatureMetadata('a', c.FeatureMetadata.Type.DISCRETE))
        feature_metadata.append(c.FeatureMetadata('b', c.FeatureMetadata.Type.DISCRETE))
        feature_metadata.append(c.FeatureMetadata('c', c.FeatureMetadata.Type.DISCRETE))
        self.run_naive_bayes_classifier(2, feature_metadata, train_data, train_labels, test_data, test_labels)


    # Test gaussian calculator

    def test_gaussian_standard_distribution(self):
        gaussian = nb.GaussianCalculator()
        self.assertAlmostEqual(gaussian.gaussian_cdf(0),    0.5,           5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(1),    0.5 + 0.34134, 5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(2.5),  0.5 + 0.49379, 5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(-2.5), 0.5 - 0.49379, 5)

        # self.assertAlmostEqual(gaussian.get_value_prob(0), 0.5 - 0.49379, 5)
        # self.assertAlmostEqual(gaussian.get_value_prob(-2.5), 0.5 - 0.49379, 5)



    def test_gaussian_custom_distribution(self):
        gaussian = nb.GaussianCalculator(5, 5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(5),     0.5,           5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(10),    0.5 + 0.34134, 5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(17.5),  0.5 + 0.49379, 5)
        self.assertAlmostEqual(gaussian.gaussian_cdf(-7.5),  0.5 - 0.49379, 5)


    # Test continuous inputs

    def test_bayes_one_continuous_feature(self):
        train_data   = [{ 'a' : 0.1 }, { 'a' : 0.2 }, { 'a' : 0.8 }, { 'a' : 0.9 }]
        train_labels = [0, 0, 1, 1]
        test_data    = [{ 'a' : 0.15 }, { 'a' : 0.85 }]
        test_labels  = [0, 1]

        feature_metadata = []
        feature_metadata.append(c.FeatureMetadata('a', c.FeatureMetadata.Type.CONTINUOUS))
        self.run_naive_bayes_classifier(2, feature_metadata, train_data, train_labels, test_data, test_labels)


# Main method
if __name__ == '__main__':
    unittest.main()