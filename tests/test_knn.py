import unittest
from src import knn
from src import classifier as c


# Naive Bayes Unit Tests

class TestKNearestNeighbors(unittest.TestCase):

    # Classifier test function

    def run_knn_classifier(self, num_classes, feature_metadata, train_data, train_labels, test_data, test_labels):
        classifier = knn.KNearestNeighborClassifier(num_classes, feature_metadata)
        classifier.train(train_data, train_labels)

        for i, test_vec in enumerate(test_data):
            prediction = classifier.classify(test_vec)
            self.assertEqual(prediction, test_labels[i])


    # Test discrete inputs

    def test_knn_one_feature(self):
        train_data   = [{ 'a' : 0 }, { 'a' : 1 }]
        train_labels = [0, 1]
        test_data    = [{ 'a' : 0 }, { 'a' : 1 }]
        test_labels  = [0, 1]

        feature_metadata = []
        feature_metadata.append(c.FeatureMetadata('a', c.FeatureMetadata.Type.DISCRETE))
        self.run_knn_classifier(2, feature_metadata, train_data, train_labels, test_data, test_labels)


# Main method
if __name__ == '__main__':
    unittest.main()